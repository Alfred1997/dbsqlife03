package dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContacto {
    private Context context;
    private AgendaDbHelper agendaDbHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[] {
            DefinirTabla.Contactos._ID,
            DefinirTabla.Contactos.NOMBRE,
            DefinirTabla.Contactos.TELEFONO1,
            DefinirTabla.Contactos.TELEFONO2,
            DefinirTabla.Contactos.DIRECCION,
            DefinirTabla.Contactos.NOTAS,
            DefinirTabla.Contactos.FAVORITO
    };
    public AgendaContacto(Context context){
        this.context = context;
        this.agendaDbHelper = new AgendaDbHelper(this.context);
    }
    public void openDatabase() { db = agendaDbHelper.getWritableDatabase(); }

    public long insertarContacto(Contactos c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contactos.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contactos.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contactos.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contactos.DIRECCION, c.getDireccion());
        values.put(DefinirTabla.Contactos.NOTAS, c.getNotas());
        values.put(DefinirTabla.Contactos.FAVORITO, c.isFavorite());

        return db.insert(DefinirTabla.Contactos.TABLA_NAME, null, values);
    }
    public long actualizarContacto(Contactos c, long id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contactos.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contactos.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contactos.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contactos.DIRECCION, c.getDireccion());
        values.put(DefinirTabla.Contactos.NOTAS, c.getNotas());
        values.put(DefinirTabla.Contactos.FAVORITO, c.isFavorite());

        String criterio = DefinirTabla.Contactos._ID + " = " + id;

        return db.update(DefinirTabla.Contactos.TABLA_NAME, values, criterio, null);
    }
    public long eliminarContacto(long id){
        String criterio = DefinirTabla.Contactos._ID + " = " + id;

        return db.delete(DefinirTabla.Contactos.TABLA_NAME, criterio, null);
    }

    public Contactos leerContacto(Cursor cursor){
        Contactos c = new Contactos();

        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDireccion(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorite(cursor.getInt(6));

        return c;
    }
    public Contactos getContacto(long id){
        Contactos contacto = null;
        SQLiteDatabase db = this.agendaDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Contactos.TABLA_NAME, columnToRead,
                DefinirTabla.Contactos._ID + " = ? ",
                new String[] { String.valueOf(id) },
                null, null, null);

        if(c.moveToFirst()){
            contacto = leerContacto(c);
        }
        c.close();
        return contacto;
    }

    public ArrayList<Contactos> allContactos() {
        ArrayList<Contactos> contactos = new ArrayList<>();
        Cursor cursor = db.query(DefinirTabla.Contactos.TABLA_NAME,
                null, null, null,
                null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Contactos c = leerContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }

        cursor.close();
        return contactos;
    }

    public void cerrar() { agendaDbHelper.close(); }

}
