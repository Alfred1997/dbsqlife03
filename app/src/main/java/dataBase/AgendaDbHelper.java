package dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AgendaDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_CONTACTO = "CREATE TABLE " +
            DefinirTabla.Contactos.TABLA_NAME + " (" +
            DefinirTabla.Contactos._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Contactos.NOMBRE + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contactos.DIRECCION + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contactos.TELEFONO1 + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contactos.TELEFONO2 + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contactos.NOTAS + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Contactos.FAVORITO + INTEGER_TYPE +
            ") ";
    private static final String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " +
            DefinirTabla.Contactos.TABLA_NAME;
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "agenda.db";

    public AgendaDbHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //super.onDowngrade(db, oldVersion, newVersion);
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CONTACTO);
    }


}

