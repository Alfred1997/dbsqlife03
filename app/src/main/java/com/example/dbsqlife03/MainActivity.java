package com.example.dbsqlife03;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import dataBase.AgendaContacto;
import dataBase.Contactos;
import dataBase.DefinirTabla;

public class MainActivity extends AppCompatActivity {

    private EditText edtNombre;
    private EditText edtTelefono1;
    private EditText edtTelefono2;
    private EditText edtDomicilio;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contactos savedContact;

    //private int savedIndex
    private int id;
    private AgendaContacto db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.txtNombre);
        edtTelefono1 = (EditText) findViewById(R.id.txtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        edtDomicilio = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas = (EditText) findViewById(R.id.txtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        db = new AgendaContacto(MainActivity.this);

        //Boton Guardar
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().equals("") ||
                        edtDomicilio.getText().toString().equals("") ||
                        edtTelefono1.getText().toString().equals("")){

                    Toast.makeText(MainActivity.this,"Favor de llenar todos los campos", Toast.LENGTH_SHORT).show();

                }
                else{
                    //Guardar o Modificar
                    Contactos nContacto = new Contactos();

                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setTelefono1(edtTelefono1.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setDireccion(edtDomicilio.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    if(cbxFavorito.isChecked()){
                        nContacto.setFavorite(1);
                    }else{
                        nContacto.setFavorite(0);
                    }
                    db.openDatabase();

                    if(savedContact == null){
                        long idx = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this, "Se agrego contacto con ID:" + idx, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        db.actualizarContacto(nContacto, id);
                        Toast.makeText(MainActivity.this, "Se actualizo el registro: " + id, Toast.LENGTH_SHORT).show();
                    }
                    db.cerrar();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                startActivityForResult(intent, 0);
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resulCode, @Nullable Intent data){
        if(Activity.RESULT_OK == resulCode){
            Contactos contacto = (Contactos) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = (int) contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtDomicilio.setText(contacto.getDireccion());
            edtTelefono1.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtNotas.setText(contacto.getNotas());
            if(contacto.getFavorite() > 0){
                cbxFavorito.setChecked(true);
            }
            else{
                limpiar();
            }
        }

    }

    public void limpiar() {
        edtNombre.setText("");
        edtTelefono1.setText("");
        edtTelefono2.setText("");
        edtDomicilio.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
        savedContact = null;
    }
}

